﻿module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Общая информация',
      collapsed: false,
      items: [
        'robossembler-overview',
        'plan',
        'replication'
      ],
    },
    {
      type: 'category',
      label: 'Компоненты',
      collapsed: false,
      items: [
        'autostorage',
        'information/information_support',
        'information/planner',
	      'techinstruction'
      ],
    },
    {
      type: 'category',
      label: 'Технологии',
      collapsed: false,
      items: [
        'technologies/photopolymer',
      ],
    },
    {
      type: 'category',
      label: 'Разное',
      collapsed: false,
      items: [
        'glossary',
        'analogy',
      ],
    },
  ],
};
