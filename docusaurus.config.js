/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Robossembler',
  tagline: 'Self Replicated Robotics Infrastructure Documentation',
  url: 'https://robosphere.gitlab.io/robossembler-docs',
  baseUrl: '/robossembler-docs/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'robossembler', // Usually your GitHub org/user name.
  projectName: 'robossembler-docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Robossembler',
      logo: {
        alt: 'Robossembler Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/robosphere/robossembler-docs',
          label: 'GitLab',
          position: 'right',
        }
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/robossembler',
            },
            {
              label: 'Discord',
              href: 'https://discord.gg/fkvKMQ3XT4',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/movefasta',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/robosphere',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Robossembler Team, Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/robosphere/robossembler-docs/-/edit/master/',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/robosphere/robossembler-docs/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
